package com.softtek.erp.infraestructure.party

import com.softtek.erp.domain.party.Person

interface PersonRepository {

  Person findOneByFirstNameAndLastName(String firstName, String lastName)
  Person findByUUID(String uuid)

  void add(Person person)
}