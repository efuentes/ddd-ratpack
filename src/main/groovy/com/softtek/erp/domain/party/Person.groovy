package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class Person extends Party {

  String firstName
  String lastName
  String gender

  def Person(String firstName, String lastName) {
    super()
    this.firstName = firstName
    this.lastName = lastName
  }

  String name() {
    return "${firstName} ${lastName}"
  }

  Person withGender(Gender gender) {
    this.gender = gender

    return this
  }

  Person withPartyRole(Term partyRoleTerm) {

    if (partyRoleTerm.vocabulary.code != 'PARTY_ROLE') {
      throw new IllegalArgumentException("Party Role Term must belong to Vocabulary 'PARTY_ROLE'.")
    }

    def isValidTerm = false
    def parent = partyRoleTerm.parent
    while (parent) {
      if (parent.code == 'PERSON_ROLE') {
        isValidTerm = true
        break
      }
      parent = parent?.parent
    }

    if (!isValidTerm) {
      throw new IllegalArgumentException("Party Role Term must be a child of 'PERSON_ROLE'.")
    }
    
    super.withPartyRole(partyRoleTerm)
  }

}

enum Gender {
  Male,
  Female,
  Undefined
}