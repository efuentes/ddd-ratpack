package com.softtek.erp.domain.party

class PartyRelationship {

  Party from
  Party to

  def PartyRelationship(Party from, Party to) {
    this.from = from
    this.to = to
  }
}