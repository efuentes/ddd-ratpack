package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class Email {

  String address
  Term type

  def Email(String address, Term type) {
    this.address = address
    this.type = type
  }
}