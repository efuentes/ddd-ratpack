package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class PartyRole {

  Term type

  def PartyRole(Term type) {
    this.type = type
  }
}