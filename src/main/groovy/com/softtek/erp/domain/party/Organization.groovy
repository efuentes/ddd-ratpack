package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.*

class Organization extends Party {

  String name
  Term type

  def Organization(String name) {
    this.name = name
  }

  Organization withType(Term type) {
    this.type = type

    return this
  }

  Organization withPartyRole(Term partyRoleTerm) {

    if (partyRoleTerm.vocabulary.code != 'PARTY_ROLE') {
      throw new IllegalArgumentException("Party Role Term must belong to Vocabulary 'PARTY_ROLE'.")
    }

    def isValidTerm = false
    def parent = partyRoleTerm.parent
    while (parent) {
      if (parent.code == 'ORGANIZATION_ROLE') {
        isValidTerm = true
        break
      }
      parent = parent?.parent
    }

    if (!isValidTerm) {
      throw new IllegalArgumentException("Party Role Term must be a child of 'ORGANIZATION_ROLE'.")
    }
    
    super.withPartyRole(partyRoleTerm)
  }
}