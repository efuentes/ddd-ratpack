package com.softtek.ddd

import static java.util.UUID.randomUUID

abstract class Entity {

  String uuid

  def Entity() {
    this.uuid = randomUUID() as String
  }

}