package com.softtek.erp.domain.taxonomy

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

import org.junit.*

class TermTests {

  @Test
  void canCreateTermWithCodeVocabularyNameWeight() {

    // Arrange
    Vocabulary organizationTypeVocabulary = new Vocabulary("ORGANIZATION_TYPE")

    // Act
    Term sut = new Term("LEGAL_ORGANIZATION", organizationTypeVocabulary).withName("Legal Organization").withWeight(10)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.code, equalTo("LEGAL_ORGANIZATION")
    assertThat sut.name, equalTo("Legal Organization")
    assertThat sut.vocabulary.code, equalTo("ORGANIZATION_TYPE")
    assertThat sut.weight, equalTo(10)
    assertThat sut.restricted, equalTo(false)
  }

  @Test
  void canCreateChildTermWithCodeVocabularyName() {

    // Arrange
    Vocabulary organizationTypeVocabulary = new Vocabulary("ORGANIZATION_TYPE")
    Term parent = new Term("LEGAL_ORGANIZATION", organizationTypeVocabulary)
    
    // Act
    Term sut = new Term("COORPORATION", parent).withName("Corporation")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.code, equalTo("COORPORATION")
    assertThat sut.name, equalTo("Corporation")
    assertThat sut.parent.code, equalTo("LEGAL_ORGANIZATION")
    assertThat sut.vocabulary.code, equalTo("ORGANIZATION_TYPE")
    assertThat sut.restricted, equalTo(false)
  }
}
