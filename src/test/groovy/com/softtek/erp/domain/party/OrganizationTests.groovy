package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.*

class OrganizationTests {
  
  @Test
  void canCreateOrganizationWithNameAndType() {

    // Arrange
    Vocabulary organizationTypeVocabulary = new Vocabulary("ORGANIZATION_TYPE")
    Term legalOrganizationTerm = new Term("LEGAL_ORGANIZATION", organizationTypeVocabulary)

    // Act
    Organization sut = new Organization("ACME, Co.").withType(legalOrganizationTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.name, equalTo("ACME, Co.")
    assertThat sut.type.code, equalTo("LEGAL_ORGANIZATION")
    assertThat sut.type.vocabulary.code, equalTo("ORGANIZATION_TYPE")
  } 

  @Test
  void canCreateOrganizationWithPartyRole() {

    // Arrange
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)

    // Act
    Organization sut = new Organization("Goodyear").withPartyRole(organizationUnitRoleTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.name, equalTo("Goodyear")
    assertThat sut.partyRoles.size(), equalTo(1)
    assertThat sut.partyRoles[0].type.code, equalTo("INTERNAL_ORGANIZATION")
  }

  @Ignore
  void canCreateOrganizationWithGeographicBoundary() {
    
  }

  @Test
  void canNotCreateOrganizationWithPartyRole_InvalidVocabulary() {
    
    // Arrange
    Throwable e = null
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_RELATIONSHIP")
    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term internalOrganizationRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)

    // Act
    Organization sut = new Organization("Goodyear")
    try {
      sut.withPartyRole(internalOrganizationRoleTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }

  @Test
  void canNotCreateOrganizationWithPartyRole_InvalidTerm() {

    // Arrange
    Throwable e = null
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)

    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)

    // Act
    Organization sut = new Organization("Goodyear")
    try {
      sut.withPartyRole(employeeRoleTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }

  @Test
  void canNotAddDuplicatedPartyRoleToPerson() {

    // Arrange
    Throwable e = null
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)

    // Act
    Organization sut = new Organization("Goodyear")
    sut.withPartyRole(organizationUnitRoleTerm)
    try {
      sut.withPartyRole(organizationUnitRoleTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat sut.partyRoles.size(), equalTo(1)
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }
}