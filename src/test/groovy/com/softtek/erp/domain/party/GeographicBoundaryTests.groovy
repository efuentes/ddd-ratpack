package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class GeographicBoundaryTests {

  @Test
  void canCreateGeographicBoundaryWithCodeTypeNameAbbreviation() {

    // Arrange
    def geographicBoundaryVocabulary = new Vocabulary("GEOGRAPHIC_BOUNDARY")
    def countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary).withName("Country")

    // Act
    def sut = new GeographicBoundary("MX", countryTerm).withName("México").withAbbreviation("Mex")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.code, equalTo("MX")
    assertThat sut.type.code, equalTo("COUNTRY")
    assertThat sut.name, equalTo("México")
    assertThat sut.abbreviation, equalTo("Mex")
  }

  @Test
  void canCreateGeographicBoundaryWithCodeTypeParent() {

    // Arrange
    def geographicBoundaryVocabulary = new Vocabulary("GEOGRAPHIC_BOUNDARY")
    def countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary).withName("Country")
    def geoCountryMex = new GeographicBoundary("MX", countryTerm).withName("México").withAbbreviation("Mex")
    def stateTerm = new Term("STATE", geographicBoundaryVocabulary).withName("State")

    // Act
    def sut = new GeographicBoundary("MX-DIF", stateTerm).withName("Distrito Federal").withAbbreviation("D.F.")
    sut.withParent(geoCountryMex)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.code, is("MX-DIF")
    assertThat sut.name, is("Distrito Federal")
    assertThat sut.type.code, is("STATE")
    assertThat sut.abbreviation, is("D.F.")
    assertThat sut.parent.code, is("MX")
  }

  @Test
  void canNotCreateGeographicBoundaryWithCodeType_InvalidVocabularyTerm() {

    // Arrange
    Throwable e = null

    def geographicBoundaryVocabulary = new Vocabulary("WRONG_GEOGRAPHIC_BOUNDARY")
    def countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary)

    // Act
    try {
      def sut = new GeographicBoundary("MX", countryTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }

}