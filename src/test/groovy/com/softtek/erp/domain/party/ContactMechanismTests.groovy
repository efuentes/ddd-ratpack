package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class ContactMechanismTests {

  @Test
  void canCreatePhoneNumberWithType() {

    // Arrange
    def contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")
    def phoneNumberTerm = new Term("PHONE_NUMBER_TYPE", contactMechanismTypeVocabulary)
    def mobilePhoneTerm = new Term("MOBILE", phoneNumberTerm)

    // Act
    PhoneNumber sut = new PhoneNumber("234-0958", mobilePhoneTerm).withAreaCode("55").withCountryCode("52")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.number, is("234-0958")
    assertThat sut.areaCode, is("55")
    assertThat sut.countryCode, is("52")
    assertThat sut.type.code, is("MOBILE")
  }

  @Test
  void canCreateEmailWithType() {

    // Arrange
    def contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")
    def emailTerm = new Term("EMAIL_TYPE", contactMechanismTypeVocabulary)
    def personalTerm = new Term("PERSONAL", emailTerm)

    // Act
    Email sut = new Email("john.smith@mail.com", personalTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.address, is("john.smith@mail.com")
    assertThat sut.type.code, is("PERSONAL")
  }

  @Ignore
  void canNotCreatePhoneNumberWithInvalidType() {

  }

  @Ignore
  void canNotCreateEmailWithInvalidType() {

  }
}