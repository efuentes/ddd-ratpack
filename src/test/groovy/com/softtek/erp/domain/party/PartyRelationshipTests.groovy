package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class PartyRelationshipTests {

  @Test
  void canCreatePartyRelationshipBetweenPersonAndOrganization() {

    // Arrange
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)
    Person employee = new Person("John", "Smith").withPartyRole(employeeRoleTerm)

    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)
    Organization internalOrganization = new Organization("Goodyear").withPartyRole(organizationUnitRoleTerm)

    // Act
    PartyRelationship sut = new PartyRelationship(internalOrganization, employee)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.from, equalTo(internalOrganization)
    assertThat sut.to, equalTo(employee)
  }

}