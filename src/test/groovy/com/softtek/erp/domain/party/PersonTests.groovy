package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class PersonTests {

  @Test
  void canCreatePersonWithFullName() {
    
    // Arrange

    // Act
    Person sut = new Person("John", "Smith")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), equalTo("John Smith")
  }

  @Test
  void canCreatePersonWithGender() {

    // Arrange
    
    // Act
    Person sut = new Person("John", "Smith").withGender(Gender.Male)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), equalTo("John Smith")
    assertThat sut.gender, equalTo("Male")
  }

  @Test
  void canCreatePersonWithPartyRole() {

    // Arrange
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE" ,personRoleTerm)

    // Act
    Person sut = new Person("John", "Smith").withPartyRole(employeeRoleTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), equalTo("John Smith")
    assertThat sut.partyRoles.size(), equalTo(1)
    assertThat sut.partyRoles[0].type.code, equalTo("EMPLOYEE")
  }

  @Test
  void canAddManyPartyRolesToPerson() {

    // Arrange
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)
    Term contractorRoleTerm = new Term("CONTRACTOR", personRoleTerm)

    // Act
    Person sut = new Person("John", "Smith")
    sut.withPartyRole(employeeRoleTerm)
    sut.withPartyRole(contractorRoleTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), equalTo("John Smith")
    assertThat sut.partyRoles.size(), equalTo(2)
    assertThat sut.partyRoles[0].type.code, equalTo("EMPLOYEE")
    assertThat sut.partyRoles[1].type.code, equalTo("CONTRACTOR")
  }

  @Test
  void canCreatePersonWithPostalAddresses() {

    // Arrange
    def geographicBoundaryVocabulary = new Vocabulary("GEOGRAPHIC_BOUNDARY")
    def countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary).withName("Country")
    def geoCountryMex = new GeographicBoundary("MX", countryTerm).withName("México").withAbbreviation("Mex")
    def stateTerm = new Term("STATE", geographicBoundaryVocabulary).withName("State")
    def geoStateMexDF = new GeographicBoundary("MX-DIF", stateTerm).withParent(geoCountryMex)
    geoStateMexDF.withName("Distrito Federal").withAbbreviation("D.F.")
    PostalAddress postalAddress1 = new PostalAddress("Address 1.1", geoStateMexDF).withAddress2("Address 1.2")
    PostalAddress postalAddress2 = new PostalAddress("Address 2.1", geoStateMexDF).withAddress2("Address 2.2")

    // Act
    Person sut = new Person("John", "Smith").withPostalAddress(postalAddress1)
    sut.withPostalAddress(postalAddress2)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), is("John Smith")
    assertThat sut.postalAddresses.size(), is(2)
    assertThat sut.postalAddresses[0].address1, is("Address 1.1")
    assertThat sut.postalAddresses[0].address2, is("Address 1.2")
    assertThat sut.postalAddresses[0].geographicBoundary.code, is("MX-DIF")
    assertThat sut.postalAddresses[1].address1, is("Address 2.1")
    assertThat sut.postalAddresses[1].address2, is("Address 2.2")
    assertThat sut.postalAddresses[1].geographicBoundary.code, is("MX-DIF")
  }

  @Test
  void canCreatePersonWithPhoneNumbersAsContactMechanism() {

    // Arrange
    def contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")
    def phoneNumberTerm = new Term("PHONE_NUMBER_TYPE", contactMechanismTypeVocabulary)
    def mobilePhoneTerm = new Term("MOBILE", phoneNumberTerm)
    def homePhoneTerm = new Term("HOME", phoneNumberTerm)
    PhoneNumber phone1 = new PhoneNumber("234-0958", mobilePhoneTerm).withAreaCode("55").withCountryCode("52")
    PhoneNumber phone2 = new PhoneNumber("234-5643", homePhoneTerm).withAreaCode("55").withCountryCode("52")

    def contactMechanismPurposeTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_PURPOSE_TYPE")
    def phoneNumberPurposeTerm = new Term("PHONE_NUMBER_PURPOSE_TYPE", contactMechanismTypeVocabulary)
    def generalPhoneNumberTerm = new Term("GENERAL_PHONE_NUMBER", phoneNumberPurposeTerm)
    def mainPhoneOfficeNumberTerm = new Term("MAIN_PHONE_OFFICE_NUMBER", phoneNumberPurposeTerm)

    // Act
    Person sut = new Person("John", "Smith")
    sut.withContactMechanism(phone1, generalPhoneNumberTerm)
    sut.withContactMechanism(phone2, mainPhoneOfficeNumberTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), is("John Smith")
    assertThat sut.contactMechanisms.size(), is(2)
    assertThat sut.contactMechanisms[0].phone.number, is("234-0958")
    assertThat sut.contactMechanisms[0].phone.countryCode, is("52")
    assertThat sut.contactMechanisms[0].phone.areaCode, is("55")
    assertThat sut.contactMechanisms[0].phone.type.code, is("MOBILE")
    assertThat sut.contactMechanisms[0].purpose.code, is("GENERAL_PHONE_NUMBER")
    assertThat sut.contactMechanisms[1].phone.number, is("234-5643")
    assertThat sut.contactMechanisms[1].phone.countryCode, is("52")
    assertThat sut.contactMechanisms[1].phone.areaCode, is("55")
    assertThat sut.contactMechanisms[1].phone.type.code, is("HOME")
    assertThat sut.contactMechanisms[1].purpose.code, is("MAIN_PHONE_OFFICE_NUMBER")
  }

  @Test
  void canCreatePersonWithEmailsAsContactMechanism() {

    // Arrange
    def contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")
    def emailTerm = new Term("EMAIL_TYPE", contactMechanismTypeVocabulary)
    def personalTerm = new Term("PERSONAL", emailTerm)
    def workTerm = new Term("WORK", emailTerm)
    Email email1 = new Email("john.smith@mail.com", personalTerm)
    Email email2 = new Email("john.smith@softtek.com", workTerm)

    def contactMechanismPurposeTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_PURPOSE_TYPE")
    def emailPurposeTerm = new Term("EMAIL_PURPOSE_TYPE", contactMechanismTypeVocabulary)
    def generalEmailTerm = new Term("PERSONAL_EMAIL", emailPurposeTerm)
    def workEmailTerm = new Term("WORK_EMAIL", emailPurposeTerm)

    // Act
    Person sut = new Person("John", "Smith")
    sut.withContactMechanism(email1, generalEmailTerm)
    sut.withContactMechanism(email2, workEmailTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), is("John Smith")
    assertThat sut.contactMechanisms.size(), is(2)
    assertThat sut.contactMechanisms[0].email.address, is("john.smith@mail.com")
    assertThat sut.contactMechanisms[0].email.type.code, is("PERSONAL")
    assertThat sut.contactMechanisms[0].purpose.code, is("PERSONAL_EMAIL")
    assertThat sut.contactMechanisms[1].email.address, is("john.smith@softtek.com")
    assertThat sut.contactMechanisms[1].email.type.code, is("WORK")
    assertThat sut.contactMechanisms[1].purpose.code, is("WORK_EMAIL")

  }

  @Test
  void canCreatePersonWithPhoneNumbersAndEmailsAsContactMechanism() {
    // Arrange
    def contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")

    def phoneNumberTerm = new Term("PHONE_NUMBER_TYPE", contactMechanismTypeVocabulary)
    def mobilePhoneTerm = new Term("MOBILE", phoneNumberTerm)
    def homePhoneTerm = new Term("HOME", phoneNumberTerm)
    PhoneNumber phone1 = new PhoneNumber("234-0958", mobilePhoneTerm).withAreaCode("55").withCountryCode("52")
    PhoneNumber phone2 = new PhoneNumber("234-5643", homePhoneTerm).withAreaCode("55").withCountryCode("52")

    def emailTerm = new Term("EMAIL_TYPE", contactMechanismTypeVocabulary)
    def personalTerm = new Term("PERSONAL", emailTerm)
    def workTerm = new Term("WORK", emailTerm)
    Email email1 = new Email("john.smith@mail.com", personalTerm)
    Email email2 = new Email("john.smith@softtek.com", workTerm)

    def contactMechanismPurposeTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_PURPOSE_TYPE")

    def phoneNumberPurposeTerm = new Term("PHONE_NUMBER_PURPOSE_TYPE", contactMechanismTypeVocabulary)
    def generalPhoneNumberTerm = new Term("GENERAL_PHONE_NUMBER", phoneNumberPurposeTerm)
    def mainPhoneOfficeNumberTerm = new Term("MAIN_PHONE_OFFICE_NUMBER", phoneNumberPurposeTerm)

    def emailPurposeTerm = new Term("EMAIL_PURPOSE_TYPE", contactMechanismTypeVocabulary)
    def generalEmailTerm = new Term("PERSONAL_EMAIL", emailPurposeTerm)
    def workEmailTerm = new Term("WORK_EMAIL", emailPurposeTerm)

    // Act
    Person sut = new Person("John", "Smith")
    sut.withContactMechanism(phone1, generalPhoneNumberTerm)
    sut.withContactMechanism(email1, generalEmailTerm)
    sut.withContactMechanism(phone2, mainPhoneOfficeNumberTerm)
    sut.withContactMechanism(email2, workEmailTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), is("John Smith")
    assertThat sut.contactMechanisms.size(), is(4)
    assertThat sut.contactMechanisms[0].phone.number, is("234-0958")
    assertThat sut.contactMechanisms[0].phone.countryCode, is("52")
    assertThat sut.contactMechanisms[0].phone.areaCode, is("55")
    assertThat sut.contactMechanisms[0].phone.type.code, is("MOBILE")
    assertThat sut.contactMechanisms[1].email.address, is("john.smith@mail.com")
    assertThat sut.contactMechanisms[1].email.type.code, is("PERSONAL")
    assertThat sut.contactMechanisms[2].phone.number, is("234-5643")
    assertThat sut.contactMechanisms[2].phone.countryCode, is("52")
    assertThat sut.contactMechanisms[2].phone.areaCode, is("55")
    assertThat sut.contactMechanisms[2].phone.type.code, is("HOME")
    assertThat sut.contactMechanisms[3].email.address, is("john.smith@softtek.com")
    assertThat sut.contactMechanisms[3].email.type.code, is("WORK")
  }

  @Test
  void canCreatePersonWithPostalAddressAsContactMechanism() {
    // Arrange
    def geographicBoundaryVocabulary = new Vocabulary("GEOGRAPHIC_BOUNDARY")
    def countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary).withName("Country")
    def geoCountryMex = new GeographicBoundary("MX", countryTerm).withName("México").withAbbreviation("Mex")
    def stateTerm = new Term("STATE", geographicBoundaryVocabulary).withName("State")
    def geoStateMexDF = new GeographicBoundary("MX-DIF", stateTerm).withParent(geoCountryMex)
    geoStateMexDF.withName("Distrito Federal").withAbbreviation("D.F.")
    PostalAddress postalAddress1 = new PostalAddress("Address 1.1", geoStateMexDF).withAddress2("Address 1.2")
    PostalAddress postalAddress2 = new PostalAddress("Address 2.1", geoStateMexDF).withAddress2("Address 2.2")

    def contactMechanismPurposeTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_PURPOSE_TYPE")
    def postalAddressPurposeTerm = new Term("POSTAL_ADDRESS_PURPOSE_TYPE", contactMechanismPurposeTypeVocabulary)
    def homeAddressTerm = new Term("HOME_ADDRESS", postalAddressPurposeTerm)
    def workAddressTerm = new Term("WORK_ADDRESS", postalAddressPurposeTerm)

    // Act
    Person sut = new Person("John", "Smith")
    sut.withContactMechanism(postalAddress1, homeAddressTerm)
    sut.withContactMechanism(postalAddress2, workAddressTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, notNullValue()
    assertThat sut.name(), is("John Smith")
    assertThat sut.contactMechanisms.size(), is(2)
    assertThat sut.contactMechanisms[0].address.address1, is("Address 1.1")
    assertThat sut.contactMechanisms[0].address.address2, is("Address 1.2")
    assertThat sut.contactMechanisms[0].address.geographicBoundary.code, is("MX-DIF")
    assertThat sut.contactMechanisms[0].purpose.code, is("HOME_ADDRESS")
    assertThat sut.contactMechanisms[1].address.address1, is("Address 2.1")
    assertThat sut.contactMechanisms[1].address.address2, is("Address 2.2")
    assertThat sut.contactMechanisms[1].address.geographicBoundary.code, is("MX-DIF")
    assertThat sut.contactMechanisms[1].purpose.code, is("WORK_ADDRESS")
  }

  @Test
  void canNotCreatePersonWithPartyRole_InvalidVocabulary() {
    
    // Arrange
    Throwable e = null

    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_RELATIONSHIP")
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)
    
    // Act
    Person sut = new Person("John", "Smith")
    try {
      sut.withPartyRole(employeeRoleTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }

  @Test
  void canNotCreatePersonWithPartyRole_InvalidTerm() {

    // Arrange
    Throwable e = null

    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)

    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)

    // Act
    Person sut = new Person("John", "Smith")
    try {
      sut.withPartyRole(organizationUnitRoleTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }

  @Test
  void canNotAddDuplicatedPartyRoleToPerson() {

    // Arrange
    Throwable e = null

    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)

    // Act
    Person sut = new Person("John", "Smith")
    sut.withPartyRole(employeeRoleTerm)
    try {
      sut.withPartyRole(employeeRoleTerm)
    } catch (Throwable ex) {
      e = ex
    }

    // Assert
    assertThat sut.partyRoles.size(), equalTo(1)
    assertThat(e instanceof IllegalArgumentException, equalTo(true))
  }

}