import static ratpack.groovy.Groovy.ratpack

ratpack {

  bindings {
    //add new JacksonModule()
  }

  handlers {
    get {
      render "Hello Ratpack with DDD!"
    }

    get(":name") {
        render "Hello $pathTokens.name!"
    }
  }

}